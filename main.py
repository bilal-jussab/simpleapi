"""
Simple API for VueJS Example
Author: Bilal Jussab
"""

#######################################
# IMPORTS
#######################################
# Standard Imports
import datetime
import random

# 3rd Party Library Imports
from flask import Flask, jsonify, request
from flask_cors import CORS


#######################################
# SETUP
#######################################
app = Flask(__name__)
app.secret_key = 'abcdefghijklmnop'
CORS(app)


#######################################
# MAIN / END POINTS
#######################################
class Values:

    def __init__(self):
        self.timestamps = []
        self.values = []

    def xy_1(self):
        base = datetime.datetime.today()
        self.timestamps = [base - datetime.timedelta(days=x) for x in range(10)]
        self.values = random.sample(range(50, 250), 10)

    def xy_1_values(self):
        self.xy_1()
        data = []
        for i, x in enumerate(self.timestamps):
            row = {'timestamp': x, 'value': self.values[i]}
            data.append(row)
        return data

    def xy_2(self):
        base = datetime.datetime.today()
        self.timestamps = [base - datetime.timedelta(days=x) for x in range(10)]
        self.values = random.sample(range(0, 10), 10)

    def xy_2_values(self):
        self.xy_2()
        data = []
        for i, x in enumerate(self.timestamps):
            row = {'timestamp': x, 'value': self.values[i]}
            data.append(row)
        return data

#######################################
# END POINTS
#######################################
@app.route('/values_1', methods=['GET'])
def get_values_1():
    v = Values()
    return jsonify(v.xy_1_values())


@app.route('/values_2', methods=['GET'])
def get_values_2():
    v = Values()
    return jsonify(v.xy_2_values())


#######################################
# RUN
#######################################
if __name__ == '__main__':
    app.run(port=420, debug=False)